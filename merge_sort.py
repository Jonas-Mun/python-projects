
def mergeSort(array):
    if len(array) > 1:
        mid = len(array) // 2
        
        left = array[:mid]
        right = array[mid:]

        left_sorted = mergeSort(left)
        right_sorted = mergeSort(right)

        sorted_array = merge(left_sorted, right_sorted)

        return sorted_array
    else:
        return array

def merge(l, r):
    sorted_array = []
    cur_pos = 0

    l_index = 0
    r_index = 0

    l_len = len(l)
    r_len = len(r)

    for x in range(0,l_len):
        print("Printing Left")
        print(l[x])

    for x in range(0, r_len):
        print("Printing Right")
        print(r[x])

    while l_index < l_len and r_index < r_len:
        print("Checking Values: ")
        print(l[l_index])
        print(r[r_index])
        if l[l_index] < r[r_index]:
            sorted_array.append(l[l_index])
            cur_pos += 1

            l_index += 1
        elif r[r_index] < l[l_index]:
            sorted_array.append(r[r_index])
            cur_pos += 1

            r_index += 1



    print("Printing sorted array")
    for x in range(0,len(sorted_array)):
        print(sorted_array[x])


    if l_index != l_len:
        for x in range(l_index, l_len):
            sorted_array.append( l[x])
            cur_pos += 1
    else:
        for x in range(r_index, r_len):
            sorted_array.append(r[x])
            cur_pos += 1

    return sorted_array

my_array = [2,5,3,1]
sorted_array = mergeSort(my_array)

print("Sorted Array")
for x in range(0,len(sorted_array)):
    print(sorted_array[x])

