from SVGDataCreator import SVGDataCreator

kanji_size = (8,8)

kanji_data = "kanji_japan/1_kanji.json"
kanji_data_2 = "kanji_japan/2_kanji.json"
kanji_data_3 = "kanji_japan/3_kanji.json"

kanji_svg = SVGDataCreator()

kanji_svg.create_svg_drawing(kanji_data, "1_kanji_japan.svg", kanji_size)
kanji_svg.create_svg_drawing(kanji_data_2, "2_kanji_japan.svg", kanji_size)
kanji_svg.create_svg_drawing(kanji_data_3, "3_kanji_japan.svg", kanji_size)

"""

kanji_svg.init_drawing("1_kanji_japan.svg")
kanji_svg.paint_paths()
kanji_svg.create_svg()

kanji_svg.new_drawing()
kanji_svg.init_drawing("2_kanji_japan.svg")
kanji_svg.new_data_set(kanji_data_2)
kanji_svg.paint_paths()
kanji_svg.create_svg()

kanji_svg.new_drawing()
kanji_svg.init_drawing("3_kanji_japan.svg")
kanji_svg.new_data_set(kanji_data_3)
kanji_svg.paint_paths()
kanji_svg.create_svg()

"""
