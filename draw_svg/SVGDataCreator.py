 # ----------------------------------------------
# Generates an SVG file based on given data in the form of a JSON file.
#
# Currently Supported elements:
#   * circle
#   * path
# 
# author: Jonas Munoz Novelo
# version: 0.5
# ----------------------------------------------

# JSON Format used - v0.5
"""
{
    "Points": [(x,y),(x1,y1),...,(xN,yN)],
    "Paths": [
            [(x01,y01),(x11,y11),...,(xN1,yN1)],
            [(x02,y02),(x12,y12),...,(xN2,yN2)],
            ]
}

"""

import json
import svgwrite


# -- generate_path --
#
# parameters:
#
# @data_path_array: [[x,y],[x1,y1],...,[xN,yN]]
#
# returns:
# SVG path string

def generate_path(data_path_array):
    path_string = ""
    length_path = len(data_path_array)
    # each coordinate
    for x in range(0,length_path):
        # Origin of Path
        if x == 0:
            path_string = path_string + "M"
            
        coord_path = data_path_array[x]
        
        path_string = path_string + str(coord_path[0]) + "," + str(coord_path[1])
        
        # Add space
        if (x != length_path-1):
            path_string = path_string + " "
        
    return path_string

class SVGDataCreator():
    def __init__(self):
        print("HELLO!")

    # Data manipulator methods
    def new_data_set(self, new_data_path):
        self.dict_points = {}
        self.dict_paths = {}
        self.convert_data_to_dict(new_data_path)
    
    def convert_data_to_dict(self, data):
        json_data = {}
        with open(data, "r") as file:
            json_data = json.load(file)
        self.convert_points(json_data["Points"])
        self.convert_paths(json_data["Paths"])

    def convert_points(self, data_points):
        for point in data_points:
            point_coord = (point[0],point[1])   # (x,y)
            self.dict_points[point_coord] = (point[0],point[1])

    def convert_paths(self, data_paths):
        path_name = "path"
        path_index = 0
        for paths in data_paths:
            self.dict_paths[path_name+str(path_index)] = paths
            path_index = path_index + 1

    # Container to create an SVG file
    def create_svg_drawing(self, data, output_file, grid_size):
        self.new_data_set(data)
        self.init_drawing(output_file, grid_size)
        
        
        if (self.dict_points != {}):
            print(self.dict_points)
            self.paint_points()
        
        if (self.dict_paths != {}):
            self.paint_paths()
        
        self.save_svg()
            
    # SVG methods
    def new_drawing(self):
        self.dwg = []
    
    def init_drawing(self, name_file, svg_size):
        self.dwg = svgwrite.Drawing(name_file, size=svg_size,debug=True)
        self.size = svg_size

    def paint_points(self):
        print("In points")
        for key in self.dict_points:
            
            point_value = self.dict_points[key]
            print(point_value[0])
            print(point_value[1])
            y_val = self.size[1] - point_value[1]   # invert Y axis for user coordinates
            self.dwg.add(self.dwg.circle((point_value[0],y_val), stroke=svgwrite.rgb(10,10,16,'%')))
    
    def paint_paths(self):
        for key in self.dict_paths:
            path_created = generate_path(self.dict_paths[key])
            self.dwg.add(self.dwg.path(d=path_created, stroke="black", fill="none"))
        

    def save_svg(self):
        self.dwg.save()

    # Print data
    def print_points(self):
        print("Point data: ", self.dict_points)

    def print_paths(self):
        print("Path data: ", self.dict_paths)

