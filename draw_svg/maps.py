from SVGDataCreator import SVGDataCreator

map_size = (115, 123)

map_data = "maps/map1.json"

map_svg = SVGDataCreator()
map_svg.create_svg_drawing(map_data, "map1.svg", map_size)