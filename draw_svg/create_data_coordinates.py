import json

state = "unknown"
possible_states = ["Points", "Paths"]

dict_coordinates = { "Points": [],
                      "Paths": []  
                    }

def both_digits(x, y):
    if x.isdigit and y.isdigit:
        return True
    else:
        return False

def create_points():
    while (True):
        print("Enter x: ")
        x = input()
        if (x == "exit") :
            break;
            
        print("Enter y: ")
        y = input()
        if (y == "exit"):
            break
        
        if both_digits(x, y):
            point = { "Label": [int(x), int(y)]}
            dict_coordinates["Points"].append(point)
        else:
            print("Invalid input")

        
def create_path():
    path_storage = []
    while(True):
        print("Enter x: ")
        x = input()
        if (x == "exit"):
            break
            
        print("Enter y: ")
        y = input()
        if (y == "exit"):
            break
        
        if both_digits(x, y):
            path_coord = [int(x), int(y)]
            path_storage.append(path_coord)
        else:
            print("Invalid input")
    
    return path_storage
        
while(True):
    if state == "unknown":
        print("Please enter a known state: ")
        new_state = input()
        
        if new_state == "exit":
            break
        for x in range(0, 2):
            if new_state == possible_states[x]:
                state = new_state
                break
                
    elif state == "Paths":
        path_created = create_path()
        dict_coordinates["Paths"].append(path_created)
        state = "unknown"
    elif state == "Points":
        create_points()
        state = "unknown"
        
            
print(dict_coordinates)