Kanji Coordinates

8x8 grid coordinate

** Merely an approximation **
Displays the rounded coordinates from the JSON file. 

Template: 

(0,0)
	 * * * * * * * * *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * * * * * * * * *
 
ORIGIN: Top left of grid, based on Inkscape

1. Implemented

	 * * * * * * * * *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - o *
	 * o - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * * * * * * * * * 
 
 2. Implemented
 
	 * * * * * * * * *
	 * - - - - - - - *
	 * - o - - - o - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * o - - - - - o *
	 * - - - - - - - *
	 * - - - - - - - *
	 * * * * * * * * *
 
 3. Implemented
 
	 * * * * * * * * *
	 * - - - - - - - *
	 * o - - - - - o *
	 * - - - - - - - *
	 * - o - - - o - *
	 * - - - - - - - *
	 * o - - - - - o *
	 * - - - - - - - *
	 * * * * * * * * *
	 
4.  Not Implemented

	 o * * l * r * * o
	 * - - - - - - - *
	 * - - - - - - r *
	 * - l - - r - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 * - - - - - - - *
	 o * * * * * * * o
	 
	 o = border coordinates
	 l = left strip coordinates
	 r = right strip coordinates