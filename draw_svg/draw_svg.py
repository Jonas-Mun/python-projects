import svgwrite
import json

dwg = svgwrite.Drawing('test.svg', height=200, width=100)

datastore = {}
with open("test_data.json", 'r') as f:
    datastore = json.load(f)


# Iterate through Points
for point in datastore["Points"]:
    print(point)
    dwg.add(dwg.circle((point[0],point[1]), stroke=svgwrite.rgb(10,10,16,'%')))

# Iterate through paths

for paths in datastore["Paths"]:
    index = 0;
    path_string = ""
    point_type = ""
    point_string = ""
    print(paths)
    for coords in paths:

        point_coord = coords
        #print(point_coord)
        
        # Beginning or continous path coordinates
        if(path_string == ""):
            point_type = "M"
        else :
            path_string = path_string + ","
        
        # Build point data -> <T>x,y
        point_string = point_type + str(point_coord[0]) + ',' + str(point_coord[1])
        
        # Build path string from point data
        path_string = path_string + point_string
        
        # reset path type
        point_type = ""
        index = index + 1
        
    print(path_string)
    dwg.add(dwg.path(d=path_string))
    
dwg.add(dwg.line((0, 0), (10, 0), stroke=svgwrite.rgb(10, 10, 16, '%')))
# dwg.add(dwg.text('Test', insert=(0, 0.2), fill='red'))
my_paths = dwg.add(dwg.path(d='M10,14, 16,16, 20,20'))


#dwg.add(dwg.circle((2,2), stroke=svgwrite.rgb(10,10,16, '%')))

dwg.save()
